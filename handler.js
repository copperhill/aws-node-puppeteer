const puppeteer = require('puppeteer');
const { getChrome } = require('./chrome-script');

module.exports.screenshot = async (event) => {

  const { urlBase64, viewWidth, viewHeight, deleteCookieName, deleteCookieDomain, deleteCookiePath } = event.queryStringParameters;
  const chrome = await getChrome();
  const browser = await puppeteer.connect({
    browserWSEndpoint: chrome.endpoint,
  });
  const page = await browser.newPage();

  await page.setViewport({ width: parseInt(viewWidth, 10) || 1920, height: parseInt(viewHeight, 10) || 1080, isLandscape: true});
  var url = Buffer.from(urlBase64, 'base64').toString();
  await page.goto(url || 'https://google.com', { waitUntil: 'networkidle0' });
  if (deleteCookieName && deleteCookieName != '' && deleteCookiePath && deleteCookiePath != '' && deleteCookieDomain && deleteCookieDomain !='') {
    await page.deleteCookie({ name: deleteCookieName, domain: deleteCookieDomain, path: deleteCookiePath });
  }
  var screenshotBuffer = await page.screenshot({ fullPage: true });

  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'image/png'
    },
    body: screenshotBuffer.toString('base64'),
    isBase64Encoded: true
  };
};
